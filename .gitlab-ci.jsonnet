[{
  include: [
    { 'local': '.gitlab-ci/helmfile-pipeline.yml' },
  ],
  stages: ['init', 'validate', 'plan'],
  variables: {
    P_PROJECT: std.extVar('P_PROJECT'),
    P_REGION: std.extVar('P_REGION'),
    P_ENVIRONMENT: std.extVar('P_ENVIRONMENT'),
    ENVIRONMENT: std.extVar('TF_WORKSPACE'),
  },
  [std.extVar('TF_WORKSPACE') + ':plan']: {
    extends: '.plan',
  },
  ['all']: {
    extends: '.plan',
  }
}]
