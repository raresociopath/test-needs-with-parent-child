#!/bin/sh
export PLAN="plan-${ENVIRONMENT}.tfplan"

if [ -z $P_REGION ]
then
	export P_REGION=$(echo "$ENVIRONMENT" | cut -d '-' -f1)
fi

if [ -z $P_PROJECT ]
then
	export P_PROJECT=$(echo "$ENVIRONMENT" | cut -d '-' -f2)
fi

if [ -z $P_ENVIRONMENT ]
then
	export P_ENVIRONMENT=$(echo "$ENVIRONMENT" | cut -d '-' -f3)
fi

export TF_WORKSPACE="${P_PROJECT}-${P_REGION}-${P_ENVIRONMENT}"
export CICD_ENV="${P_ENVIRONMENT}"
